#!/bin/bash
# set -eux
set -u


VAULT_ADDR="http://127.0.0.1:8200"
opts="--address=$VAULT_ADDR"    
admin_user=${VAULT_ADMIN_USER:=admin}
admin_password=${VAULT_ADMIN_PASSWORD:=admin}

vault_bin="docker-compose exec vault vault"

## Vault Init
$vault_bin operator init $opts -status 2>&1 > /dev/null
rc=$?
echo "=============================================================="
if [ $rc -eq 2 ];then
    echo "Vault not initialized"
    $vault_bin operator init $opts -format=json> .vault_keys.json
elif [ $rc -eq 1 ];then
    echo "Vault Initialization error"
    exit 1
else 
    echo "Vault initialized"
fi

VAULT_TOKEN=$(cat .vault_keys.json | jq -r -M ".root_token")

## Vault Unseal
echo "=============================================================="
$vault_bin status $opts 2>&1 > /dev/null
rc=$?
if [ $rc -eq 2 ];then
    echo "Vault sealed"
    for i in {1..3}; do
        echo "--------------------------------------------------------------"
        echo "unseal vault with key $i"
        unseal_key=$(cat .vault_keys.json | jq -r -M ".unseal_keys_hex[$i]")
        $vault_bin operator unseal $opts "$unseal_key"
    done
elif [ $rc -eq 1 ];then
    echo "Vault unsealing error"
    exit 1
else 
    echo "Vault unsealed"
fi

## Vault status to check if initialized and unsealed
echo "=============================================================="
echo "check vault status"
$vault_bin status $opts 

## Vault root auth
echo "=============================================================="
echo "root login to Vault"
$vault_bin login $opts ${VAULT_TOKEN} 

## vault logs
$vault_bin audit list $opts  | grep file/ 2>&1 > /dev/null
if [ $? -ne 0 ];then 
    echo "=============================================================="
    echo "enable vault audit logs file"
    $vault_bin audit enable $opts file file_path=/vault/logs/audit.log
fi

## Vault users
# enable user password auth
$vault_bin auth list $opts  | grep userpass/ 2>&1 > /dev/null
if [ $? -ne 0 ];then 
    echo "=============================================================="
    echo "enable vault auth userpass"
    $vault_bin auth enable $opts userpass
fi

# create admin policies
echo "create admin policy"
$vault_bin policy write $opts admins /vault/policies/admins.hcl

# create users
echo "create admin user"
$vault_bin write $opts auth/userpass/users/${admin_user} password=${admin_password} policies=admins
