# set default shell
SHELL := $(shell which bash)
ENV = /usr/bin/env
.SILENT: ;               # no need for @
.ONESHELL: ;             # recipes execute in same shell
.NOTPARALLEL: ;          # wait for this target to finish
.EXPORT_ALL_VARIABLES: ; # send all vars to shell

# default shell options
DKC = docker-compose
DK = docker

opt = 

ADMIN_USER=admin
ADMIN_PASSWORD=admin

default: all;   # default target

all: volumes
	$(MAKE) rm
	$(MAKE) build
	$(MAKE) up
	$(MAKE) vault_init
.PHONY: all

volumes: 
	mkdir -p volumes
.PHONY: volumes

_rmdata:
	rm -rf volumes/* .vault_keys.json
.PHONY: _rmdata 

build:
	$(ENV) $(DKC) build
.PHONY: build

rm: stop
rm:
	$(ENV) $(DKC) rm -f -v
.PHONY: rm

stop:
	$(ENV) $(DKC) stop
.PHONY: stop

up: _upd
up: ps
.PHONY: up

_upd: volumes
	$(ENV) $(DKC) up -d
.PHONY: _upd

ps:
	$(ENV) $(DKC) ps
.PHONY: ps

log:
	$(ENV) $(DKC) logs $(opt)
.PHONY: log

config:
	$(ENV) $(DKC) config $(opt)
.PHONY: config

check:
	$(ENV) $(DKC) config -q
.PHONY: check

down: 
	$(ENV) $(DKC) down -v --remove-orphans
	$(MAKE) _rmdata
.PHONY: down

## vault subcommand

vault_init:
	export VAULT_ADMIN_USER="$(ADMIN_USER)"
	export VAULT_ADMIN_PASSWORD="${ADMIN_PASSWORD}"
	bash ./scripts/init.sh
.PHONY: vault_init
