Poc Vault
=========

POC https://www.vaultproject.io/ with docker-compose


Requirements
------------

* GNU Make
* docker
  * docker-compose
* jq


Usage
-----

### launch and initialize the vault server

```sh
make all
```

### Destroy the servers and the data

```sh
make down
```
